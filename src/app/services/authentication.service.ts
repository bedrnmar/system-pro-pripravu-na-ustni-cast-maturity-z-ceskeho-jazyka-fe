import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../dto/environment';
import {NotificationService} from './notification.service';
import {User} from '../dto/user';

@Injectable({ providedIn: 'root' })
export class AuthenticationService {


  constructor(private http: HttpClient,
              private notificationService: NotificationService) {
  }

  public login(username: string, password: string){
    this.http.post(environment.publicUrl + '/login', { username, password })
      .subscribe(
        data => {
          const user: User = data as User;
          console.log(user);
          localStorage.setItem('userToken', user.jwt);
          localStorage.setItem('username', user.username);
          localStorage.setItem('userRole', user.roles[0]);
          localStorage.setItem('userId', user.id.toString());
          this.notificationService.notifySuccess('Úspěšně přihlášeno.');
        },
        error => {
          console.error('Couldn\'t post because', error);
          if (error.status !== 401 && error.status !== 200) {
            this.notificationService.notifyError('Chyba při přihlášení. ' + error.status);
          }
        }
      );
  }

  getToken(): string {
    return localStorage.getItem('userToken');
  }

  isUserLogged(): boolean{
    if (localStorage.getItem('username') !== null){
      return true;
    } else {
      return false;
    }
  }

  logout() {
    localStorage.removeItem('userToken');
    localStorage.removeItem('username');
    localStorage.removeItem('userRole');
    localStorage.removeItem('userId');
  }
}
