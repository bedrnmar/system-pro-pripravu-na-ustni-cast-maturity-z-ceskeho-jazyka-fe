import { Injectable } from '@angular/core';
import {MatSnackBar} from '@angular/material/snack-bar';

@Injectable({
  providedIn: 'root'
})
export class NotificationService {

  constructor(private snackBar: MatSnackBar) {
  }

  public notifySuccess(text: string){
    this.snackBar.open(text, 'Zavřít', {
      duration: 3000
    });
  }

  public notifyError(text: string){
    this.snackBar.open(text, 'Zavřít', {
      panelClass: ['error-notify']
    });
  }
}
