import { TestBed } from '@angular/core/testing';

import { EventTransportService } from './event-transport.service';

describe('EventTransportService', () => {
  let service: EventTransportService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(EventTransportService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
