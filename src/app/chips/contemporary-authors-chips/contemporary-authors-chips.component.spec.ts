import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ContemporaryAuthorsChipsComponent } from './contemporary-authors-chips.component';

describe('ContemporaryAuthorsChipsComponent', () => {
  let component: ContemporaryAuthorsChipsComponent;
  let fixture: ComponentFixture<ContemporaryAuthorsChipsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ContemporaryAuthorsChipsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ContemporaryAuthorsChipsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
