import {Component, ElementRef, EventEmitter, Input, OnInit, Output, ViewChild} from '@angular/core';
import {COMMA, ENTER} from '@angular/cdk/keycodes';
import {Observable} from 'rxjs';
import {FormControl} from '@angular/forms';
import {MatAutocomplete, MatAutocompleteSelectedEvent} from '@angular/material/autocomplete';
import {map, startWith} from 'rxjs/operators';
import {MatChipInputEvent} from '@angular/material/chips';
import {BookService} from '../../book/book.service';
import {ContextRequest} from '../../dto/contextRequest';
import {NotificationService} from '../../services/notification.service';

@Component({
  selector: 'app-contemporary-authors-chips',
  templateUrl: './contemporary-authors-chips.component.html',
  styleUrls: ['./contemporary-authors-chips.component.css']
})
export class ContemporaryAuthorsChipsComponent implements OnInit {
  separatorKeysCodes: number[] = [ENTER, COMMA];
  filteredAuthors: Observable<string[]>;
  allContemporaryAuthors: string[];
  authorsControl = new FormControl();

  @ViewChild('authorInput') authorInput: ElementRef<HTMLInputElement>;
  @ViewChild('auto') matAutocomplete: MatAutocomplete;

  contemporaryAuthors: string[] = [];
  @Output() contemporaryAuthorsChanged: EventEmitter<string[]> = new EventEmitter();
  @Input() authorNameIn: string;
  @Input() firstEditionIn: number;
  @Input() czechOrWorld: string;

  constructor(private bookService: BookService,
              private notificationService: NotificationService) {
  }

  add(event: MatChipInputEvent): void {
    const input = event.input;
    const value = event.value;

    if ((value || '').trim()) {
      this.contemporaryAuthors.push(value.trim());
    }

    if (input) {
      input.value = '';
    }

    this.authorsControl.setValue(null);
    this.contemporaryAuthorsChanged.emit(this.contemporaryAuthors);
  }

  remove(author: string): void {
    const index = this.contemporaryAuthors.indexOf(author);

    if (index >= 0) {
      this.contemporaryAuthors.splice(index, 1);
    }
    this.allContemporaryAuthors.push(author);
    this.contemporaryAuthorsChanged.emit(this.contemporaryAuthors);
  }

  selected(event: MatAutocompleteSelectedEvent): void {
    this.contemporaryAuthors.push(event.option.viewValue);
    this.authorInput.nativeElement.value = '';
    this.authorsControl.setValue(null);

    const index = this.allContemporaryAuthors.indexOf(event.option.viewValue);
    if (index >= 0) {
      this.allContemporaryAuthors.splice(index, 1);
    }

    this.contemporaryAuthorsChanged.emit(this.contemporaryAuthors);
  }

  private _filter(value: string): string[] {
    const filterValue = value.toLowerCase();

    return this.allContemporaryAuthors.filter(genre => genre.toLowerCase().indexOf(filterValue) === 0);
  }

  ngOnInit(): void {

  }

  loadContemporaryAuthors() {
    const request: ContextRequest = {
      authorName: this.authorNameIn,
      firstEdition: this.firstEditionIn,
      isCzech: this.czechOrWorld === 'czech'
    };
    this.bookService.getContemporaryAuthors(request)
      .subscribe(
        data => {
          this.allContemporaryAuthors = data;
          this.notificationService.notifySuccess('Nalezeno ' + this.allContemporaryAuthors.length.toString() + ' soudobých autorů');
          let i = 0;
          for (const author of this.allContemporaryAuthors) {
            this.contemporaryAuthors.push(author);
            ++i;
            if (i > 5){
              break;
            }
          }

          this.filteredAuthors = this.authorsControl.valueChanges.pipe(
            startWith(null),
            map((genre: string | null) => genre ? this._filter(genre) : this.allContemporaryAuthors.slice()));
        },
        error => {
          this.notificationService.notifyError('Nepodařilo se nalézt žádné soudobé autory ' + error);
        }
      );
  }

  setContemporaryAuthors(authorsObtained: string[]){
    for (const author of authorsObtained){
      this.contemporaryAuthors.push(author);
    }
  }
}
