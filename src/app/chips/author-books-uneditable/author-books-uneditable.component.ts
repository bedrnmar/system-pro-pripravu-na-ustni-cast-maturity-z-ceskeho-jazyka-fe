import {Component, OnInit} from '@angular/core';
import {Observable} from 'rxjs';

@Component({
  selector: 'app-author-books-uneditable',
  templateUrl: './author-books-uneditable.component.html',
  styleUrls: ['./author-books-uneditable.component.css']
})
export class AuthorBooksUneditableComponent implements OnInit {
  filteredGenres: Observable<string[]>;

  constructor() {
  }

  books: string[] = [];

  ngOnInit(): void {
  }

  setBooks(booksObtained: string[]){
    if (booksObtained !== undefined) {
      this.books = booksObtained;
    } else {
      this.books = [];
    }
  }

  getLabel(): string{
    if (this.books === undefined  || this.books === null || this.books.length < 1){
      return 'Databáze neobsahuje žádné rozebrané dílo tohoto autora';
    } else {
      return 'Díla autora';
    }
  }
}
