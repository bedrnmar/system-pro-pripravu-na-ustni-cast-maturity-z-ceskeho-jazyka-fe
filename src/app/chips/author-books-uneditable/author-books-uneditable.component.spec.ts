import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AuthorBooksUneditableComponent } from './author-books-uneditable.component';

describe('AuthorBooksUneditableComponent', () => {
  let component: AuthorBooksUneditableComponent;
  let fixture: ComponentFixture<AuthorBooksUneditableComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AuthorBooksUneditableComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AuthorBooksUneditableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
