import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {COMMA, ENTER} from '@angular/cdk/keycodes';
import {Observable} from 'rxjs';
import {MatChipInputEvent} from '@angular/material/chips';
import {AuthorService} from '../../author/author.service';

@Component({
  selector: 'app-authors-books',
  templateUrl: './authors-books.component.html',
  styleUrls: ['./authors-books.component.css']
})
export class AuthorsBooksComponent implements OnInit {
  separatorKeysCodes: number[] = [ENTER, COMMA];
  filteredGenres: Observable<string[]>;

  constructor() {
  }

  books: string[] = [];
  @Input() isRequired = false;
  @Input() isEditable = true;
  @Output() booksChanged: EventEmitter<string[]> = new EventEmitter();

  add(event: MatChipInputEvent): void {
    const input = event.input;
    const value = event.value;

    if (this.books === null){
      this.books = [];
    }
    if ((value || '').trim()) {
      this.books.push(value.trim());
    }

    if (input) {
      input.value = '';
    }
    this.booksChanged.emit(this.books);
  }

  remove(book: string): void {
    const index = this.books.indexOf(book);

    if (index >= 0) {
      this.books.splice(index, 1);
    }
    this.booksChanged.emit(this.books);
  }

  ngOnInit(): void {
  }

  addStarForRequired(): string{
    if (this.isRequired){
      return ' *';
    } else {
      return '';
    }
  }

  getPlaceholder(): string{
    if (this.isEditable){
      return 'Přidejte díla..';
    } else {
      return '';
    }
  }

  setBooks(booksObtained: string[]){
    if (booksObtained !== undefined) {
      this.books = booksObtained;
    } else {
      this.books = [];
    }
  }

}
