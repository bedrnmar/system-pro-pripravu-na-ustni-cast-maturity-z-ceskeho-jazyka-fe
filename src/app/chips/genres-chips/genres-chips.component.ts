import {Component, ElementRef, EventEmitter, Input, OnInit, Output, ViewChild} from '@angular/core';
import {map, startWith} from 'rxjs/operators';
import {MatChipInputEvent} from '@angular/material/chips';
import {MatAutocomplete, MatAutocompleteSelectedEvent} from '@angular/material/autocomplete';
import {COMMA, ENTER} from '@angular/cdk/keycodes';
import {Observable} from 'rxjs';
import {FormControl} from '@angular/forms';

@Component({
  selector: 'app-genres-chips',
  templateUrl: './genres-chips.component.html',
  styleUrls: ['./genres-chips.component.css']
})
export class GenresChipsComponent implements OnInit {
  separatorKeysCodes: number[] = [ENTER, COMMA];
  filteredGenres: Observable<string[]>;
  allGenres: string[];
  generesControl = new FormControl();

  @ViewChild('genreInput') genreInput: ElementRef<HTMLInputElement>;
  @ViewChild('auto') matAutocomplete: MatAutocomplete;

  genres: string[] = [];
  @Output() genresChanged: EventEmitter<string[]> = new EventEmitter();

  constructor() {
    this.filteredGenres = this.generesControl.valueChanges.pipe(
    startWith(null),
    map((genre: string | null) => genre ? this._filter(genre) : this.allGenres.slice()));
  }

  add(event: MatChipInputEvent): void {
    const input = event.input;
    const value = event.value;

    if ((value || '').trim()) {
      this.genres.push(value.trim());
    }

    if (input) {
      input.value = '';
    }

    this.generesControl.setValue(null);
    this.genresChanged.emit(this.genres);
  }

  remove(genre: string): void {
    const index = this.genres.indexOf(genre);

    if (index >= 0) {
      this.genres.splice(index, 1);
    }
    this.allGenres.push(genre);

    this.genresChanged.emit(this.genres);
  }

  selected(event: MatAutocompleteSelectedEvent): void {
    this.genres.push(event.option.viewValue);
    this.genreInput.nativeElement.value = '';
    this.generesControl.setValue(null);

    const index = this.allGenres.indexOf(event.option.viewValue);
    if (index >= 0) {
      this.allGenres.splice(index, 1);
    }

    this.genresChanged.emit(this.genres);
  }

  private _filter(value: string): string[] {
    const filterValue = value.toLowerCase();

    return this.allGenres.filter(genre => genre.toLowerCase().indexOf(filterValue) === 0);
  }

  ngOnInit(): void {
    this.getAllGenres();
  }

  getAllGenres() {
    this.allGenres = ['Anekdota', 'Antiutopie', 'Bajka', 'Balada', 'Cestopis', 'Detektivka',
      'Elegie', 'Epigram', 'Epos', 'Fantasy', 'Kaligram', 'Komedie', 'Kronika',
      'Legenda', 'Mýtus', 'Novela', 'Óda', 'Pásmo', 'Píseň', 'Poema',
      'Pohádka', 'Pověst', 'Povídka', 'Román', 'Sci-fi', 'Sonet',
      'Romance', 'Tragédie', 'Utopie'
    ];
  }


  setCurrentGenres(genres: string[]){
    this.genres = genres;
  }

}
