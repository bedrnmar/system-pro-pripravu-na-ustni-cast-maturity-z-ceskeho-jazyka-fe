import { Component } from '@angular/core';
import {EventTransportService} from './services/event-transport.service';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  clickEventSub: Subscription;
  title = 'UstniFE';
  name = 'APPNAME';
  opened = true;

  constructor(private eventService: EventTransportService) {
    this.clickEventSub = this.eventService.getClickEvent().subscribe( () => {
        this.toggleSideNavBar();
      });
  }

  toggleSideNavBar(){
    this.opened = !this.opened;
  }

}
