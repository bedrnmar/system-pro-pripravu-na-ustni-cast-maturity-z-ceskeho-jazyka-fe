import {Component, OnInit} from '@angular/core';
import {MatDialog, MatDialogConfig} from '@angular/material/dialog';
import {RegisterDialogComponent} from '../dialogs/register-dialog/register-dialog.component';
import {HttpClient} from '@angular/common/http';
import {User} from '../dto/user';
import {LoginDialogComponent} from '../dialogs/login-dialog/login-dialog.component';
import {ConfirmDialogComponent} from '../dialogs/confirm-dialog/confirm-dialog.component';
import {AuthenticationService} from '../services/authentication.service';
import {EventTransportService} from '../services/event-transport.service';
import {NotificationService} from '../services/notification.service';
import {environment} from '../dto/environment';

@Component({
  selector: 'app-toolbar',
  templateUrl: './toolbar.component.html',
  styleUrls: ['./toolbar.component.css']
})
export class ToolbarComponent implements OnInit {

  constructor(private dialog: MatDialog,
              private http: HttpClient,
              private authService: AuthenticationService,
              private eventService: EventTransportService,
              private notificationService: NotificationService) { }

  ngOnInit(): void {
  }

  openRegistrationDialog() {
    const dialogConfig = new MatDialogConfig();

    dialogConfig.autoFocus = true;
    const dialogRef = this.dialog.open(RegisterDialogComponent, dialogConfig);

    dialogRef.afterClosed().subscribe(
      data => {
        console.log(data);
        this.http.post<User>(environment.publicUrl + '/register', data)
          .subscribe(
            data2 => {
              this.notificationService.notifySuccess('Uživatel úspešně vytvořen');
            },
            error => {
              this.notificationService.notifyError('Uživatele se nepodařilo vytvořit');
            }
          );
      });
  }

  openLoginDialog() {
    const dialogConfig = new MatDialogConfig();

    dialogConfig.autoFocus = true;
    const dialogRef = this.dialog.open(LoginDialogComponent, dialogConfig);

    dialogRef.afterClosed().subscribe(
      data => this.authService.login(data.username, data.password));
  }

  logout() {
    const dialogConfig = new MatDialogConfig();

    dialogConfig.autoFocus = true;
    dialogConfig.width = '500px';
    dialogConfig.data = {
      title: 'Odhlášení uživatele',
      text: 'Chcete se odhlásit?'
    };

    const dialogRef = this.dialog.open(ConfirmDialogComponent, dialogConfig);

    dialogRef.afterClosed().subscribe(
      data => {
        if (data === true) {
          this.authService.logout();
        }
      });
  }

  toggleSideNav() {
    this.eventService.sendClickEvent();
  }

  isUserLogged(): boolean{
    if (this.getUsername() !== null){
      return true;
    } else {
      return false;
    }
  }

  getUsername(): string{
    return localStorage.getItem('username');
  }

  showUsername(): string{
    if (this.isUserLogged() === true){
      return this.getUsername().toUpperCase();
    } else{
      return '';
    }
  }
}
