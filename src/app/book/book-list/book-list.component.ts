import {Component, OnInit, AfterViewInit, ViewChild} from '@angular/core';
import {BookService} from '../book.service';
import {MatTableDataSource} from '@angular/material/table';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {Router} from '@angular/router';
import {BookView} from '../../dto/bookView';
import {NotificationService} from '../../services/notification.service';

@Component({
  selector: 'app-book-list',
  templateUrl: './book-list.component.html',
  styleUrls: ['./book-list.component.css']
})
export class BookListComponent implements OnInit, AfterViewInit{
  displayedColumns: string[] = ['name', 'author', 'form', 'category'];
  dataSource;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(private bookService: BookService,
              private router: Router,
              private notificationService: NotificationService) {}

  ngOnInit(): void {
    this.dataSource = new MatTableDataSource<BookView>();
    this.bookService.getBooks()
      .subscribe(
        data => {
          this.dataSource.data = data;
        },
        error => {
          console.error('Couldn\'t post because', error);
          if (error.status !== 401 && error.status !== 200) {
            this.notificationService.notifyError('Chyba při načítání. ' + error.status);
          }
        }
      );
  }

  ngAfterViewInit(): void {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  openBookDetail(bookId: number){
      this.router.navigate(['./book-detail', bookId]);
  }

  translateForm(form: string): string{
    if (form === 'DRAMA'){
      return 'Drama';
    } else if (form === 'PROSE') {
      return 'Próza';
    } else if (form === 'POETRY') {
      return 'Poezie';
    } else {
      return form;
    }
  }

  translateCategory(category: string): string{
    if (category === 'UNTIL18'){
      return 'Česká a světová literatura do konce 18. století';
    } else if (category === 'ALL19') {
      return 'Česká a světová literatura 19. století';
    } else if (category === 'CZECH20'){
      return 'Česká literatura 20. a 21 století';
    } else if (category === 'WORLD20') {
      return 'Světová literatura 20. a 21. století';
    }
  }
}
