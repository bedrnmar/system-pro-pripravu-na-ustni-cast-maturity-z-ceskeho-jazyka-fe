import {Component, OnInit, ViewChild} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {BookService} from '../book.service';
import {ActivatedRoute, Params, Router} from '@angular/router';
import {Book} from '../../dto/book';
import {AuthorDetail} from '../../dto/authorDetail';
import {NotificationService} from '../../services/notification.service';
import {BookCharacter} from '../../dto/bookCharacter';
import {MatDialog, MatDialogConfig} from '@angular/material/dialog';
import {ContextDialogComponent} from '../../dialogs/context-dialog/context-dialog.component';
import {AuthorsBooksComponent} from '../../chips/authors-books/authors-books.component';
import {AuthorBooksUneditableComponent} from '../../chips/author-books-uneditable/author-books-uneditable.component';
import {GenresChipsComponent} from '../../chips/genres-chips/genres-chips.component';
import {ContemporaryAuthorsChipsComponent} from '../../chips/contemporary-authors-chips/contemporary-authors-chips.component';
import {CharactersComponent} from '../characters/characters.component';
import {MatStepper} from '@angular/material/stepper';

@Component({
  selector: 'app-edit-book',
  templateUrl: './edit-book.component.html',
  styleUrls: ['./edit-book.component.css']
})
export class EditBookComponent implements OnInit {

  @ViewChild(AuthorsBooksComponent) authorBooks: AuthorsBooksComponent;
  @ViewChild(AuthorBooksUneditableComponent) uneditableBooks: AuthorBooksUneditableComponent;
  @ViewChild(GenresChipsComponent) genreChipsComponent: GenresChipsComponent;
  @ViewChild(ContemporaryAuthorsChipsComponent) contemporaryAuthorsChipsComponent: ContemporaryAuthorsChipsComponent;
  @ViewChild(CharactersComponent) charactersComponent: CharactersComponent;
  @ViewChild('stepper') myStepper: MatStepper;

  bookForm: FormGroup;
  id: number;
  book: Book;

  litForms: string[] = ['Próza', 'Drama', 'Poezie'];

  bookNameControl = new FormControl(null, [Validators.required, Validators.maxLength(80)]);
  litFormControl = new FormControl(null,  [Validators.required]);
  litGenresControl = new FormControl();
  firstEditionControl = new FormControl(null, [Validators.required, Validators.pattern('^[0-9]*$')]);
  isCzechOrWorldControl = new FormControl(null, [Validators.required]);
  authorNameControl = new FormControl(null,  [Validators.maxLength(80)]);
  authorInfoControl = new FormControl();
  authorBooksDbControl = new FormControl();
  authorBooksOtherControl = new FormControl();
  historyContextControl = new FormControl();
  literaryContextControl = new FormControl();
  contemporaryAuthorsControl = new FormControl();
  themeControl = new FormControl();
  motivesControl = new FormControl();
  plotControl = new FormControl();
  spaceControl = new FormControl();
  timeControl = new FormControl();
  charactersControl = new FormControl();
  compositionControl = new FormControl();
  extraInfoControl = new FormControl();

  controls = [this.bookNameControl, this.litFormControl, this.litGenresControl, this.firstEditionControl, this.isCzechOrWorldControl,
    this.authorNameControl, this.authorInfoControl, this.authorBooksOtherControl,
    this.historyContextControl, this.literaryContextControl,
    this.contemporaryAuthorsControl, this.themeControl, this.motivesControl, this.plotControl, this.spaceControl,
    this.timeControl, this.charactersControl, this.compositionControl, this.extraInfoControl];

  progressCounter = 0;
  progressBarValue = 0;
  progressBarColor;
  maxProgress = 17;

  constructor(private formBuilder: FormBuilder,
              private bookService: BookService,
              private router: Router,
              private route: ActivatedRoute,
              private dialog: MatDialog,
              private notificationService: NotificationService) {
  }

  ngOnInit(): void {
    if (this.route.routeConfig.component.name === this.constructor.name) {
      this.route.params.subscribe((params: Params) => {
        this.id = params.id;
      });
    }

    this.bookForm = this.formBuilder.group({
      bookId: this.id,
      bookName: this.bookNameControl,
      litForm: this.litFormControl,
      litGenres: this.litGenresControl,
      firstEdition: this.firstEditionControl,
      isCzechOrWorld: this.isCzechOrWorldControl,
      authorName: this.authorNameControl,
      authorInfo: this.authorInfoControl,
      authorBooksDb: this.authorBooksDbControl,
      authorBooksOther: this.authorBooksOtherControl,
      historyContext: this.historyContextControl,
      literaryContext: this.literaryContextControl,
      contemporaryAuthors: this.contemporaryAuthorsControl,
      theme: this.themeControl,
      motives: this.motivesControl,
      space: this.spaceControl,
      time: this.timeControl,
      plot: this.plotControl,
      characters: this.charactersControl,
      composition: this.compositionControl,
      extraInfo: this.extraInfoControl
    });
    this.getBook();
  }

  getBook(){
    this.bookService.getBook(this.id)
      .subscribe(
        data => {
          this.book = data;
          console.log(this.book);
          this.bookNameControl.setValue(this.book.bookName);
          this.litFormControl.setValue(this.findLitFormValue(this.book.litForm));
          this.litGenresControl.setValue(this.book.litGenres);
          this.genreChipsComponent.setCurrentGenres(this.book.litGenres);
          this.firstEditionControl.setValue(this.book.firstEdition);
          this.isCzechOrWorldControl.setValue(this.findOriginValue(this.book.isCzechOrWorld));
          this.authorNameControl.setValue(this.book.authorName);
          this.authorInfoControl.setValue(this.book.authorInfo);
          this.authorBooksDbControl.setValue(this.book.authorBooksDb);
          this.uneditableBooks.setBooks(this.book.authorBooksDb);
          this.authorBooksOtherControl.setValue(this.book.authorBooksOther);
          this.authorBooks.setBooks(this.book.authorBooksOther);
          this.historyContextControl.setValue(this.book.historyContext);
          this.literaryContextControl.setValue(this.book.literaryContext);
          this.contemporaryAuthorsControl.setValue(this.book.contemporaryAuthors);
          this.contemporaryAuthorsChipsComponent.setContemporaryAuthors(this.book.contemporaryAuthors);
          this.themeControl.setValue(this.book.theme);
          this.motivesControl.setValue(this.book.motives);
          this.spaceControl.setValue(this.book.space);
          this.timeControl.setValue(this.book.time);
          this.plotControl.setValue(this.book.plot);
          this.charactersControl.setValue(this.book.characters);
          this.charactersComponent.setCharacters(this.book.characters);
          this.compositionControl.setValue(this.book.composition);
          this.extraInfoControl.setValue(this.book.extraInfo);
          this.progressChanged();
        }, error => {
          this.notificationService.notifyError('Knihu se nepodařilo načíst ' + error);
          this.router.navigate(['./book-list']);
        }
      );
  }

  authorChangedHandler(author: AuthorDetail) {
    if (author.id !== null) {
      this.authorNameControl.setValue(author.name);
      this.authorInfoControl.setValue(author.info);
      this.authorBooksDbControl.setValue(author.dbBooks);
      this.authorBooksOtherControl.setValue(author.otherBooks);
      if (this.uneditableBooks !== undefined) {
        this.uneditableBooks.setBooks(author.dbBooks);
      }
      if (this.authorBooks !== undefined) {
        this.authorBooks.setBooks(author.otherBooks);
      }
      this.authorNameControl.disable();
    } else{
      this.authorNameControl.setValue(null);
      this.authorInfoControl.setValue(null);
      this.authorBooksDbControl.setValue(null);
      this.authorBooksOtherControl.setValue(null);
      this.uneditableBooks.setBooks(null);
      this.authorBooks.setBooks(null);
      this.authorNameControl.enable();
    }
  }

  genresChangedHandler(genres: string[]) {
    this.litGenresControl.setValue(genres);
  }

  booksChangedHandler(otherBooks: string[]) {
    this.authorBooksOtherControl.setValue(otherBooks);
  }

  authorsChangedHandler(authors: string[]) {
    this.contemporaryAuthorsControl.setValue(authors);
  }

  charactersChangedHandler(characters: BookCharacter[]) {
    this.charactersControl.setValue(characters);
  }

  copyContext(type: string){
    const dialogConfig = new MatDialogConfig();

    dialogConfig.autoFocus = true;
    dialogConfig.data = {
      contextType: type,
      name: this.authorNameControl.value,
      firstEdition: this.firstEditionControl.value,
      isCzech: this.isCzechOrWorldControl
    };

    const dialogRef = this.dialog.open(ContextDialogComponent, dialogConfig);

    dialogRef.afterClosed().subscribe(
      data => {
        if (type === 'history'){
          this.historyContextControl.setValue(data);
        } else if (type === 'literary'){
          this.literaryContextControl.setValue(data);
        }
      });
  }

  progressChanged(){
    this.progressCounter = 0;
    this.controls.forEach((e) => {
      if (e.value !== null && e.value !== '' && e.value !== []){
        this.progressCounter++;
      }
    });
    if (this.authorNameControl.disabled === true){
      this.progressCounter++;
    }
    this.progressBarValue = this.progressCounter * 5.3;
    if (this.progressBarValue < 33){
      this.progressBarColor = 'warn';
    } else if (this.progressBarValue < 66){
      this.progressBarColor = 'accent';
    } else {
      this.progressBarColor = 'primary';
    }
  }

  isAuthorFilled(): boolean{
    if (this.authorNameControl.value === null || this.authorNameControl.value === ''){
      this.maxProgress = 16;
      return false;
    } else {
      this.maxProgress = 19;
      return true;
    }
  }

  updateBook(){
    this.bookService.updateBook(this.id, this.bookForm.value);
  }

  findLitFormValue(formValue: string){
    if (formValue === 'PROSE'){
      return 'Próza';
    } else if (formValue === 'POETRY'){
      return 'Poezie';
    } else {
      return 'Drama';
    }
  }

  findOriginValue(formValue: string){
    if (formValue === 'WORLD'){
      return 'world';
    } else {
      return 'czech';
    }
  }

}
