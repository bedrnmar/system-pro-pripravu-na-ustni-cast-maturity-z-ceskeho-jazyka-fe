import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Params, Router} from '@angular/router';
import {Book} from '../../dto/book';
import {BookService} from '../book.service';
import {MatDialog, MatDialogConfig} from '@angular/material/dialog';
import {ConfirmDialogComponent} from '../../dialogs/confirm-dialog/confirm-dialog.component';
import {NotificationService} from '../../services/notification.service';
import {AuthenticationService} from '../../services/authentication.service';

@Component({
  selector: 'app-book-detail',
  templateUrl: './book-detail.component.html',
  styleUrls: ['./book-detail.component.css']
})
export class BookDetailComponent implements OnInit {
  id: number;
  book: Book = {
    id: null,
    bookName: null,
    litForm: null,
    litGenres: null,
    firstEdition: null,
    isCzechOrWorld: null,
    authorName: null,
    authorInfo: null,
    authorBooksDb: null,
    authorBooksOther: null,
    historyContext: null,
    literaryContext: null,
    contemporaryAuthors: null,
    theme: null,
    motives: null,
    space: null,
    time: null,
    plot: null,
    characters: null,
    composition: null,
    extraInfo: null
  };

  constructor(private router: Router,
              private route: ActivatedRoute,
              private bookService: BookService,
              private dialog: MatDialog,
              private notificationService: NotificationService,
              private auth: AuthenticationService){
  }

  ngOnInit(): void {
    if (this.route.routeConfig.component.name === this.constructor.name) {
      this.route.params.subscribe((params: Params) => {
        this.id = params.id;
        this.getBook();
      });
    } else {
      this.notificationService.notifyError('Nevalidní id rozboru');
    }
  }

  getBook(){
    this.bookService.getBook(this.id)
      .subscribe(
        data => {
          this.book = data;
        },
        error => {
          this.notificationService.notifyError('Rozbor se nepodařilo načíst ' + error);
          this.router.navigate(['./book-list']);
        }
      );
  }

  showArrayInBlock(array: string[]): string{
    let text = '';

    if (array !== undefined && array !== null) {
      for (let i = 0; i < array.length; i++) {
        if (array[i] !== this.book.bookName) {
          text = text + array[i];

          if (i !== array.length - 1) {
            text = text + ', ';
          }
        }
      }
    }
    return text;
  }

  showArrayAsParagraphs(array: string[]): string{
    let text = '';

    for (let i = 0; i < array.length; i++) {
      text = text + array[i];
      if (i !== array.length - 1){
        text = text;
      }
    }
    return text;
  }

  translateForm(form: string): string {
    if (form === 'DRAMA') {
      return 'Drama';
    } else if (form === 'PROSE') {
      return 'Próza';
    } else if (form === 'POETRY') {
      return 'Poezie';
    } else {
      return form;
    }
  }

  countCategory(): string{
    if (this.book.firstEdition <= 1800){
      return 'Světová a česká literatura do konce 18. století';
    } else if (this.book.firstEdition <= 1900){
      return 'Světová a česká literatura 19. století';
    } else if (this.book.isCzechOrWorld === 'Czech'){
      return 'Česká literatura 20. a 21. století';
    } else{
      return 'Světová literatura 20. a 21. století';
    }
  }

  editBook(): void{
    this.router.navigate(['./edit-book', this.id]);
  }

  deleteBook(){
    const dialogConfig = new MatDialogConfig();

    dialogConfig.autoFocus = true;
    dialogConfig.width = '500px';
    dialogConfig.data = {
      title: 'Smazání rozboru',
      text: 'Opravdu chcete smazat rozbor titulu ' + this.book.bookName + '?'
    };

    const dialogRef = this.dialog.open(ConfirmDialogComponent, dialogConfig);

    dialogRef.afterClosed().subscribe(
      data => {
        console.log(data);
        if (data === true) {
          this.bookService.deleteBook(this.id);
        }
      });
  }

  showInfo(value): string{
    if (value === undefined || value === null || value === '' || value === []) {
      return 'Tato sekce neobsahuje žádné informace. Můžete je doplnit pomocí tlačítka Upravit rozbor.';
    } else {
      return value;
    }
  }

  isUserLoggedIn(): boolean{
    return this.auth.isUserLogged();
  }

}
