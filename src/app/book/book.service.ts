import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Book} from '../dto/book';
import {NotificationService} from '../services/notification.service';
import {BookView} from '../dto/bookView';
import {environment} from '../dto/environment';
import {Observable} from 'rxjs';
import {BookContext} from '../dto/bookContext';
import {ContextRequest} from '../dto/contextRequest';
import {Router} from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class BookService {

  constructor(private http: HttpClient,
              private notificationService: NotificationService,
              private router: Router) { }

  getBooks(): Observable<BookView[]>{
    return this.http.get<BookView[]>(environment.publicUrl + '/book-list');
  }

  getContexts( type: string, contextRequest: ContextRequest ): Observable<BookContext[]>{
    if (type === 'history') {
      return this.http
        .post<BookContext[]>(environment.publicUrl + '/book-history', contextRequest);
    } else if (type === 'literary') {
      return this.http
        .post<BookContext[]>(environment.publicUrl + '/book-literary', contextRequest);
    }
  }

  getContemporaryAuthors( contextRequest: ContextRequest): Observable<string[]>{
    return this.http.post<string[]>(environment.publicUrl + '/contemporary-authors', contextRequest);
  }

  getBook( id: number): Observable<Book>{
    return this.http.get<Book>(environment.publicUrl + '/book-detail?id=' + id);
  }

  createBook( book: Book){
    this.http.post<Book>(environment.publicUrl + '/create-book', book)
      .subscribe(
        data => {
          this.notificationService.notifySuccess('Úspěšně vytvořeno.');
          this.router.navigate(['/book-list']);
        },
        error => {
          console.error('Couldn\'t post because', error);
          if (error.status !== 401 && error.status !== 200) {
            this.notificationService.notifyError('Chyba při ukládání. ' + error.status);
          }
        }
      );
  }

  updateBook(id: number, book: Book){
    this.http.put<Book>(environment.publicUrl + '/edit-book?id=' + id, book)
      .subscribe(
        data => {
          this.notificationService.notifySuccess('Úspěšně změněno.');
          this.router.navigate(['/book-list']);
        },
        error => {
          console.error('Couldn\'t update because', error);
          this.notificationService.notifyError('Chyba při ukládání. ' + error.status);
        }
      );
  }

  deleteBook( id: number) {
    this.http.delete(environment.privateUrl + '/delete-book?id=' + id)
      .subscribe(
        data => {
          this.notificationService.notifySuccess('Úspěšně smazáno.');
          this.router.navigate(['./book-list']);
        },
        error => {
          console.error('Couldn\'t post because', error);
          if (error.status !== 401 && error.status !== 200) {
            this.notificationService.notifyError('Chyba při ukládání. ' + error.status);
          }
        }
      );
  }
}

/*const COMPLETE_BOOK_DATA: Book[] = [
  {id: 1, bookName: 'Hobit', litForm: 'Próza', litGenres: ['Fantasy', 'Novela'], firstEdition: 1937, isCzechOrWorld: 'World', authorName: 'J. R. R. Tolkien',
    authorInfo: 'John Ronald Reuel Tolkien CBE FRSL (3 January 1892 – 2 September 1973) was an English writer, poet, philologist, and academic, best known as the author of the high fantasy works The Hobbit and The Lord of the Rings.
    He served as the Rawlinson and Bosworth Professor of Anglo-Saxon and Fellow of Pembroke College, Oxford from 1925 to 1945 and the Merton Professor of English Language and Literature and Fellow of Merton College, Oxford from 1945 to 1959. He was a close friend of C. S. Lewis, a co-member of the informal literary discussion group The Inklings. Tolkien was appointed a Commander of the Order of the British Empire by Queen Elizabeth II on 28 March 1972.
    After Tolkien's death, his son Christopher published a series of works based on his father's extensive notes and unpublished manuscripts, including The Silmarillion. These, together with The Hobbit and The Lord of the Rings, form a connected body of tales, poems, fictional histories, invented languages, and literary essays about a fantasy world called Arda and, within it, Middle-earth. Between 1951 and 1955, Tolkien applied the term legendarium to the larger part of these writings.
    While many other authors had published works of fantasy before Tolkien, the great success of The Hobbit and The Lord of the Rings led directly to a popular resurgence of the genre. This has caused Tolkien to be popularly identified as the "father" of modern fantasy literature—or, more precisely, of high fantasy. In 2008, The Times ranked him sixth on a list of "The 50 greatest British writers since 1945". Forbes ranked him the fifth top-earning "dead celebrity" in 2009. ',
    authorBooks: [Pán prstenů, Příhody Toma Bombadila, Artušův pád],
    historyContext: Evropa se vzpomatovává z první světové války, přichází válka druhá
    literaryContext: The Hobbit’s plot and characters combined the ancient heroic Anglo-Saxon and Scandinavian epics Tolkien studied with the middle-class rural England in which he lived. In many ways, the novel’s charm and humor lie in transplanting a simple, pastoral Englishman of the 1930s into a heroic medieval setting. Tolkien acknowledged that his hero, Bilbo Baggins, was patterned on the rural Englishmen of his own time.
    By the time Tolkien began to work on the sequel to The Hobbit, he had developed a friendship with another well-known Oxford professor and writer, C. S. Lewis, author of The Chronicles of Narnia. Their friendship lasted for many years. Tolkien helped convert Lewis to Christianity (although Tolkien, a Roman Catholic, was disappointed that Lewis became a Protestant), and the two critiqued each other’s work as part of an informal group of writers known as the Inklings.',
    contemporaryAuthors: [C. S. Lewis, Terry Pratchett, Ray Bradbury, George Orwell],
    theme: 'The Hobbit’s main theme is Bilbo’s development into a hero, which more broadly represents the development of a common person into a hero. At the beginning of the story, Bilbo is timid, comfortable, and complacent in his secure little hole at Bag End. When Gandalf talks him into embarking on the quest with Thorin’s dwarves, Bilbo becomes so frightened that he faints. But as the novel progresses, Bilbo prevails in the face of danger and adversity, justifying Gandalf’s early claim that there is more to the little hobbit than meets the eye.',
    motives: 'Contrasting Worldviews, The Nature and Geography of Middle-Earth',
    space: Středozemě,
    time: Odehrává se na sklonku 3. věku,
    plot:
       Bilbo Baggins lives a quiet, peaceful life in his comfortable hole at Bag End. Bilbo lives in a hole because he is a hobbit—one of a race of small, plump people about half the size of humans, with furry toes and a great love of good food and drink. Bilbo is quite content at Bag End, near the bustling hobbit village of Hobbiton, but one day his comfort is shattered by the arrival of the old wizard Gandalf, who persuades Bilbo to set out on an adventure with a group of thirteen militant dwarves. The dwarves are embarking on a great quest to reclaim their treasure from the marauding dragon Smaug, and Bilbo is to act as their “burglar.” The dwarves are very skeptical about Gandalf’s choice for a burglar, and Bilbo is terrified to leave his comfortable life to seek adventure. But Gandalf assures both Bilbo and the dwarves that there is more to the little hobbit than meets the eye.
       Shortly after the group sets out, three hungry trolls capture all of them except for Gandalf. Gandalf tricks the trolls into remaining outside when the sun comes up, and the sunlight turns the nocturnal trolls to stone. The group finds a great cache of weapons in the trolls’ camp. Gandalf and the dwarf lord Thorin take magic swords, and Bilbo takes a small sword of his own.
       The group rests at the elfish stronghold of Rivendell, where they receive advice from the great elf lord Elrond, then sets out to cross the Misty Mountains. When they find shelter in a cave during a snowstorm, a group of goblins who live in the caverns beneath the mountain take them prisoner. Gandalf leads the dwarves to a passage out of the mountain, but they accidentally leave behind Bilbo.
       Wandering through the tunnels, Bilbo finds a strange golden ring lying on the ground. He takes the ring and puts it in his pocket. Soon he encounters Gollum, a hissing, whining creature who lives in a pool in the caverns and hunts fish and goblins. Gollum wants to eat Bilbo, and the two have a contest of riddles to determine Bilbo’s fate. Bilbo wins by asking the dubious riddle, “What have I got in my pocket?”
       Gollum wants to eat Bilbo anyway, and he disappears to fetch his magic ring, which turns its wearer invisible. The ring, however, is the same one Bilbo has already found, and Bilbo uses it to escape from Gollum and flee the goblins. He finds a tunnel leading up out of the mountain and discovers that the dwarves and Gandalf have already escaped. Evil wolves known as Wargs pursue them, but Bilbo and his comrades are helped to safety by a group of great eagles and by Beorn, a creature who can change shape from a man into a bear.
       The company enters the dark forest of Mirkwood, and, making matters worse, Gandalf abandons them to see to some other urgent business. In the forest, the dwarves are caught in the webs of some giant spiders, and Bilbo must rescue them with his sword and magic ring. After slaying his first spider, Bilbo names his sword Sting. Shortly after escaping the spiders, the unlucky dwarves are captured by a group of wood elves who live near the river that runs through Mirkwood. Bilbo uses his ring to help the company escape and slips the dwarves away from the elves by hiding them inside barrels, which he then floats down the river. The dwarves arrive at Lake Town, a human settlement near the Lonely Mountain, under which the great dragon sleeps with Thorin’s treasure.
       After sneaking into the mountain, Bilbo talks to the sly dragon Smaug, who unwittingly reveals that his armorlike scales have a weak spot near his heart. When Bilbo steals a golden cup from the dragon’s hoard, Smaug is furious and flies out of the mountain to burn Lake Town in his rage. Bard, a heroic archer, has learned the secret about Smaug’s weakness from a thrush, and he fires an arrow into the dragon’s heart, killing him. Before Smaug dies, however, he burns Lake Town to the ground.
       The humans of Lake Town and the elves of Mirkwood march to the Lonely Mountain to seek a share of the treasure as compensation for their losses and aid, but Thorin greedily refuses, and the humans and elves besiege the mountain, trapping the dwarves and the hobbit inside. Bilbo sneaks out to join the humans in an attempt to bring peace. When Thorin learns what Bilbo has done, he is livid, but Gandalf suddenly reappears and saves Bilbo from the dwarf lord’s wrath.
       At this moment, an army of goblins and Wargs marches on the mountain, and the humans, elves, and dwarves are forced to band together to defeat them. The goblins nearly win, but the arrival of Beorn and the eagles helps the good armies win the battle.
       After the battle, Bilbo and Gandalf return to Hobbiton, where Bilbo continues to live. He is no longer accepted by respectable hobbit society, but he does not care. Bilbo now prefers to talk to elves and wizards, and he is deeply content to be back among the familiar comforts of home after his grand and harrowing adventures.\n',
    characters: [ 'Bilbo Baggins\n' +
    'The hero of the story. Bilbo is a hobbit, “a short, human-like person.” Commonsensical and fastidious, Bilbo leads a quiet life in his comfortable hole at Bag End and, like most hobbits, is content to stay at home. But Bilbo possesses a great deal of untapped inner strength, and when the wizard Gandalf persuades Bilbo to join a group of dwarves on a quest to reclaim their gold from a marauding dragon, Bilbo ends up playing a crucial role as the company’s burglar. Bilbo’s adventures awaken his courage and initiative and prove his relentless ability to do what needs to be done.',
      'Gandalf\n' +
      'A wise old wizard who always seems to know more than he reveals. Gandalf has a vast command of magic and tends to show up at just the moment he is needed most. Though he helps the dwarves in their quest (not least by making Bilbo go along with them), he does not seem to have any interest in their gold. He always has another purpose or plan in mind, but he rarely reveals his private thoughts.',
      'Thorin Oakenshield\n' +
      'A dwarf who leads his fellow dwarves on a trip to the Lonely Mountain to reclaim their treasure from Smaug. Smaug’s bounty is Thorin’s inheritance, as it belonged to Thror, Thorin’s grandfather, the great King under the Mountain. Thorin is a proud, purposeful, and sturdy warrior, if a bit stubborn at times. As the novel progresses, his inability to formulate successful plans, his greed, and his reliance on Bilbo to save him at every turn make Thorin a somewhat unappealing figure, but he is partly redeemed by the remorse he shows before he dies.',
      'Gollum\n' +
      'A strange, small, slimy creature who lives deep in the caves of Moria beneath the Misty Mountains. There, Gollum broods over his “precious,” a magic ring, until he accidentally loses it and Bilbo finds it. We never learn exactly what kind of creature he is. Apparently, his true shape has been too deformed by years of living in darkness to be recognizable.',
    ],
    composition: 'Kniha je rozdělena do kapitol, děj je vyprávěn chronologicky. Vševědoucí vypravěč v er-formě. Obsahuje přímou řeč',
    extraInfo: 'Neobsahuje žádné ženské postavy. Zfilmováno. Elfština založena na finsštině.'
  },
  {id: 2, bookName: 'Pán prstenů - Společenstvo prstenu', litForm: 'Próza', litGenres: ['Fantasy', 'Román'],
    firstEdition: 1954, isCzechOrWorld: 'World', authorName: 'J. R. R. Tolkien',
    authorInfo: 'John Ronald Reuel Tolkien CBE FRSL (3 January 1892 – 2 September 1973) was an English writer, poet, philologist, and academic, best known as the author of the high fantasy works The Hobbit and The Lord of the Rings.\n' +
      'He served as the Rawlinson and Bosworth Professor of Anglo-Saxon and Fellow of Pembroke College, Oxford from 1925 to 1945 and the Merton Professor of English Language and Literature and Fellow of Merton College, Oxford from 1945 to 1959.[3] He was a close friend of C. S. Lewis, a co-member of the informal literary discussion group The Inklings. Tolkien was appointed a Commander of the Order of the British Empire by Queen Elizabeth II on 28 March 1972.\n' +
      'After Tolkien\'s death, his son Christopher published a series of works based on his father\'s extensive notes and unpublished manuscripts, including The Silmarillion. These, together with The Hobbit and The Lord of the Rings, form a connected body of tales, poems, fictional histories, invented languages, and literary essays about a fantasy world called Arda and, within it, Middle-earth. Between 1951 and 1955, Tolkien applied the term legendarium to the larger part of these writings.\n' +
      'While many other authors had published works of fantasy before Tolkien, the great success of The Hobbit and The Lord of the Rings led directly to a popular resurgence of the genre. This has caused Tolkien to be popularly identified as the "father" of modern fantasy literature—or, more precisely, of high fantasy. In 2008, The Times ranked him sixth on a list of "The 50 greatest British writers since 1945". Forbes ranked him the fifth top-earning "dead celebrity" in 2009. ',
    authorBooks: ['Hobit', 'Příhody Toma Bombadila', 'Artušův pád'],
    historyContext: 'Evropa se vzpomatovává z války druhá',
    literaryContext: 'Literatura sprvky sci-fi:-velice pestrý žánr, který bývá dále členěn-fantazy literatura se někdy řadí pod sci-fi literaturu, někdy je brána zvlášť(sci-fi literatura zobrazuje technické vymoženosti, fantazy literatura bájný a pohádkový svět). K této literatuře se řadí i autoři vizí totalitních společností',
    contemporaryAuthors: ['C. S. Lewis', 'Terry Pratchett', 'Ray Bradbury', 'George Orwell'],
    theme: 'Scholars and critics have identified many themes in the book with its complex interlaced narrative, including a reversed quest, the struggle of good and evil, death and immortality, fate and free will, the addictive danger of power, and various aspects of Christianity such as the presence of three Christ figures, for prophet, priest, and king, as well as elements like hope and redemptive suffering. There is a common theme throughout the work of language, its sound, and its relationship to peoples and places, along with hints of providence in descriptions of weather and landscape. Out of these, Tolkien stated that the central theme is death and immortality. To those who supposed that the book was an allegory of events in the 20th century, Tolkien replied in the Foreword to the Second Edition that it was not, saying he preferred "history, true or feigned, with its varied applicability to the thought and experience of readers."',
    motives: 'The temptation of the ring, Mordor, Journeys',
    space: 'Středozemě', time: 'Odehrává se na sklonku 3. věku',
    plot:
      'Bilbo celebrates his 111th (eleventy-first as written in the novel) birthday and leaves the Shire suddenly without warning, leaving the Ring to Frodo Baggins, his cousin[c] and heir. Neither hobbit is aware of the Ring\'s nature, but the wizard Gandalf realises that it is a Ring of Power. Seventeen years later, Gandalf tells Frodo that he has confirmed that the Ring is the one lost by the Dark Lord Sauron long ago and counsels him to take it away from the Shire. Gandalf leaves, promising to return by Frodo\'s birthday and accompany him on his journey, but fails to do so.\n' +
      'Frodo sets out on foot, ostensibly moving to his new home in Crickhollow, accompanied by his gardener, Sam Gamgee, and his cousin, Pippin Took. They are pursued by mysterious Black Riders, but meet a passing group of Elves led by Gildor Inglorion, their chants to Elbereth warding off the Riders. The hobbits spend the night with them, then take a short cut to avoid their pursuers the next day, and arrive at the farm of Farmer Maggot. He takes them to Bucklebury Ferry, where they meet their friend Merry Brandybuck who was looking for them. When they reach the house at Crickhollow, Merry and Pippin reveal they know about the Ring and insist on travelling with Frodo and Sam. They decide to shake off the Black Riders by cutting through the Old Forest. Merry and Pippin are trapped by Old Man Willow, an evil tree who controls much of the forest, but are rescued by the mysterious Tom Bombadil. Leaving, they are caught by a barrow-wight, who traps them in a barrow on the downs. Frodo, awakening from the barrow-wight\'s spell, manages to call Bombadil, who frees them, and equips them with ancient swords from the barrow-wight\'s hoard.\n' +
      'The hobbits reach the village of Bree, where they encounter a Ranger named Strider. The innkeeper gives Frodo a letter from Gandalf written three months before which identifies Strider as a friend. Strider leads the hobbits into the wilderness after another close escape from the Black Riders, who they now know to be the Nazgûl, men of ancient times enslaved by lesser Rings of Power to serve Sauron. On the hill of Weathertop, they are again attacked by the Nazgûl, who wound Frodo with a cursed blade. Strider fights them off and leads the hobbits towards the Elven refuge of Rivendell. Frodo falls deathly ill; Strider treats him with the herb athelas, saving his life. The Nazgûl nearly capture Frodo at the Ford of Bruinen, but Strider, Sam and the Elf-lord Glorfindel drive the Nazgûl into the water, where flood waters summoned by Elrond, master of Rivendell, rise up and overwhelm them.\n',
    characters: [ 'Though in many ways Frodo is an ordinary hobbit, happy to live among his friends and family in the Shire, his pure, incorruptible heart sets him apart not only from other hobbits but also from the other races of Middle-earth and makes him the ideal candidate to deliver the ring of power to Mordor. Frodo’s mission to destroy the ring involves a treacherous journey and countless dangers, such as orcs, volcanoes, and wraiths, and in facing these obstacles he is no different from the other eight members of the fellowship. However, his task involves much more than this perilous journey to Mordor. His real challenge is to bear the ring without giving in to its temptations. This resistance is Frodo’s inner journey, in which his pure heart is constantly under assault by his darker yearnings for power. The ring tempts others in the fellowship, however good and pure they are. Gandalf, Aragorn, Sam, and Bilbo all have their eyes widen when the ring is before them, and their own weaknesses, despite their often remarkable physical strength, prove how difficult a task for Frodo carrying the ring really is. The difficulty makes his success all the more impressive.',
      'Sam views Frodo much as Frodo views the ring, as something to be protected and guided to a final destination, and Sam’s dedication makes him one of the most important members of the fellowship. While Aragorn is the star fighter of the group, it is Sam who proves the most indispensable to Frodo, and the two are so isolated in their journey that they usually don’t know what the other members of the fellowship are doing or facing. Though the other members make it possible for Frodo and Sam to continue on their journey, Sam himself makes it possible for Frodo to carry on. Sam takes his responsibilities as Frodo’s companion very seriously, and he upholds his vow never to leave Frodo even when circumstances are at their most dangerous. When an exhausted Frodo falters near the end of The Return of the King, Sam literally carries his friend the rest of the distance to Mount Doom. Sam is loyal as well as pure, and this purity helps him resist the power of the ring. Sam has countless opportunities to steal the ring from Frodo, but he takes it only when he believes Frodo is dead. He returns the ring with little hesitation, a selfless act that suggests that had Frodo actually died, Sam would have had the strength to carry out the destruction of it on his own. ',
      'The descendent of Isildur, Aragorn is the heir to the throne of Gondor, but at the beginning of the trilogy, he hides this identity and pretends to be a ranger named Strider. That Aragorn does not claim his throne, and that the steward Denethor rules Gondor, show the disunity and weakness of man at the beginning of The Fellowship of the Ring. However, Aragorn is not king because he is not yet ready. As much as the trilogy tells of Frodo’s inner steadfastness before constant temptation, it also tells of Aragorn’s transformation from ranger to king. He must grow into his position as king, and his own journey proves vital not only for his rightful coronation but for the very survival and growth of the kingdoms of man. He gains confidence and self-awareness through his courageous support of Frodo and the rest of the fellowship, as well as from his love of Arwen.',
      'Gollum\n' +
      'A strange, small, slimy creature who lives deep in the caves of Moria beneath the Misty Mountains. There, Gollum broods over his “precious,” a magic ring, until he accidentally loses it and Bilbo finds it. We never learn exactly what kind of creature he is. Apparently, his true shape has been too deformed by years of living in darkness to be recognizable.',
    ],
    composition: 'Kniha je rozdělena do kapitol, děj je vyprávěn chronologicky. Vševědoucí vypravěč v er-formě. Obsahuje přímou řeč',
    extraInfo: 'Neobsahuje žádné ženské postavy. Zfilmováno. Elfština založena na finsštině.'
  },
  {id: 3, bookName: 'R.U.R.',  litForm: 'Drama', litGenres: ['Sci-fi', 'Anti-utopie'],
    firstEdition: 1920, isCzechOrWorld: 'Czech', authorName: 'Karel Čapek',
    authorInfo: 'Karel Čapek (9 January 1890 – 25 December 1938) was a Czech writer, playwright and critic. He has become best known for his science fiction, including his novel War with the Newts (1936) and play R.U.R. (Rossum\'s Universal Robots, 1920), which introduced the word robot. He also wrote many politically charged works dealing with the social turmoil of his time. Influenced by American pragmatic liberalism,[3] he campaigned in favour of free expression and strongly opposed the rise of both fascism and communism in Europe.\n ' +
      'Though nominated for the Nobel Prize in Literature seven times, Čapek never received it. However, several awards commemorate his name, such as the Karel Čapek Prize, awarded every other year by the Czech PEN Club for literary work that contributes to reinforcing or maintaining democratic and humanist values in society. He also played a key role in establishing the Czechoslovak PEN Club as a part of International PEN.' +
      'Čapek died on the brink of World War II as the result of a lifelong medical condition, but his legacy as a literary figure became well established after the war.',
    authorBooks: ['Krakatit', 'Bílá nemoc', 'Válka s mloky'],
    historyContext: 'První světová válkaů, blíží se konec Čapka',
    literaryContext: 'Literatura s prvky sci-fi:-velice pestrý žánr, který bývá dále členěn-fantazy literatura se někdy řadí pod sci-fi literaturu, někdy je brána zvlášť(sci-fi literatura zobrazuje technické vymoženosti, fantazy literatura bájný a pohádkový svět). K této literatuře se řadí i autoři vizí totalitních společností',
    contemporaryAuthors: ['C. S. Lewis', 'Terry Pratchett', 'Ray Bradbury', 'George Orwell'],
    theme: 'Zjišťování, kdo dal robotům city.',
    motives: 'pokus, povinnost, nátlak, experiment, lež',
    space: 'Továrna Rossum\'s Universal Robots', time: 'Blíže nespecifikovaná budoucnost ',
    plot:
      'Úvodní dějství se odehrává v blíže nespecifikované budoucnosti na Rossumově ostrově výrobny robotů. Celá výroba je prováděna roboty. Připluje za nimi pěkná dcera prezidenta Helena Gloryová, která chce roboty zrovnoprávnit a do které se všichni zamilují. Postupně jí vyprávějí příběh vynálezu prvních robotů starého Rossuma a jeho synovce. Nakonec se vdá za ředitele Harryho Domina.\n' +
      'Příběh dále pokračuje po deseti letech, kdy už jsou roboti široce rozšířeni. Heleně je smutno z toho, že roboti, ač velmi podobní lidem, nemohou mít city jako lidé, a tak přiměje Dr. Galla, aby začal experimentovat s jejich „duší“. Jejich původním cílem bylo vytvořit pro člověka ráj na zemi, aby již nikdy nemuseli lidé umírat hlady a měli všeho dostatek. Jenže upadly mravy, lidé zlenivěli a začali roboty původně určené k práci používat jako vojáky pro své války.\n' +
      'Nakonec se několik robotů vzbouřilo, ustanovilo Ústřední výbor robotů a vyhlásilo válku lidstvu, jež drtivě porazilo a nenechalo nikoho přežít. Nakonec dorazili až na výrobní ostrov a pozabíjeli všechny až na Alquista, protože on ještě pracoval rukama (což ho uklidňovalo; věřil, že je práce smyslem života). Jenže roboti se začali opotřebovávat a umírali. Chtěli po něm, aby objevil původní Rossumův výrobní postup, ten však spálila Helena předtím, než ji zabili.\n' +
      'Jednou v noci Alquista vzbudili robot Primus a robotka Helena. Když chtěl jednoho z nich pitvat (kvůli výzkumu), druhý se nabízel místo něj - vůbec se nechovali jako roboti (snažili se navzájem chránit; záleželo jim jednomu na druhém). Nakonec zjistil, že se mají rádi, a tak je poslal pryč, aby udrželi život a začali s láskou znovu jako Adam a Eva.\n' +
      'Závěr je symbolickým odkazem k Bibli, z níž Alquist cituje.',
    characters: [ 'Harry Domin - Osmatřicetiletý šéf firmy. Chce, aby lidé byli někým, aby nemuseli pracovat. Místo nich by pracovali roboti, dělníci bez duše',
      'Helena  Gloryová  -  Jednatřicetiletá  Dominova  manželka.  Ztělesnění  ženského,  citového,  laskavého přístupu k robotům i lidem. Je velmi elegantní. ',
      'Dr. Gall - přednosta fyziologického a výzkumného oddělení',
      'Stavitel  Alquist  -  za  největší  hodnotu  považuje  lidský  život  a  práci,  rozumový  přístup  k  životu, lidskosti.',
    ],
    composition: 'Kniha je rozdělena do třech dějství a předmluva, děj je vyprávěn chronologicky.',
    extraInfo: 'Jména postav mají význam'
  }
];*/
