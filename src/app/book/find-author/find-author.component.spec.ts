import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FindAuthorComponent } from './find-author.component';

describe('FindAuthorComponent', () => {
  let component: FindAuthorComponent;
  let fixture: ComponentFixture<FindAuthorComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FindAuthorComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FindAuthorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
