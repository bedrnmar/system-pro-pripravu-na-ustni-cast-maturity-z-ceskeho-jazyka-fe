import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {Observable} from 'rxjs';
import {FormControl} from '@angular/forms';
import {map, startWith} from 'rxjs/operators';
import {AuthorService} from '../../author/author.service';
import {AuthorDetail} from '../../dto/authorDetail';

@Component({
  selector: 'app-find-author',
  templateUrl: './find-author.component.html',
  styleUrls: ['./find-author.component.css']
})
export class FindAuthorComponent implements OnInit {
  filteredAuthors: Observable<AuthorDetail[]>;
  allAuthors: AuthorDetail[] = [];
  authorControl = new FormControl();

  selected = false;

  empty: AuthorDetail = {
    id: null,
    name: 'Vlastní autor',
    info: null,
    dbBooks: null,
    otherBooks: null
  };

  @Output() authorChanged: EventEmitter<AuthorDetail> = new EventEmitter();

  constructor(private authorService: AuthorService) {
  }

  ngOnInit() {
    this.getAllAuthors();
  }

  displayFn(authorDetail?: AuthorDetail): string | undefined {
    return authorDetail ? authorDetail.name : undefined;
  }

  private _filter(name: string): AuthorDetail[] {
    const filterValue = name.toLowerCase();

    return this.allAuthors.filter(option => option.name.toLowerCase().indexOf(filterValue) === 0);
  }

  authorSelected(author: AuthorDetail){
    this.authorControl.setValue(author);
    this.selected = author.name !== 'Vlastní autor';
    this.authorChanged.emit(author);
  }

  getAllAuthors(): AuthorDetail[] {
    this.authorService.getAuthors()
        .subscribe(
          data => {
            this.allAuthors.push(this.empty);
            this.allAuthors = this.allAuthors.concat(data);
            this.filteredAuthors = this.authorControl.valueChanges
              .pipe(
                startWith<string | AuthorDetail>(''),
                map(value => typeof value === 'string' ? value : value.name),
                map(name => name ? this._filter(name) :  this.allAuthors.slice())
              );
            this.authorControl.setValue(this.empty);
          },
          error => {
            this.allAuthors = [];
            console.error('Couldn\'t post because', error);
          }
        );
    return this.allAuthors;
  }

}
