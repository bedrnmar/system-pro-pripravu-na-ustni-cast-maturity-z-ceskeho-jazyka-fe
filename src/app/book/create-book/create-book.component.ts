import {Component, OnInit, ViewChild} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {BookService} from '../book.service';
import {Router} from '@angular/router';
import {AuthorDetail} from '../../dto/authorDetail';
import {MatDialog, MatDialogConfig} from '@angular/material/dialog';
import {ContextDialogComponent} from '../../dialogs/context-dialog/context-dialog.component';
import {BookCharacter} from '../../dto/bookCharacter';
import {AuthorsBooksComponent} from '../../chips/authors-books/authors-books.component';
import {AuthorBooksUneditableComponent} from '../../chips/author-books-uneditable/author-books-uneditable.component';

@Component({
  selector: 'app-create-book',
  templateUrl: './create-book.component.html',
  styleUrls: ['./create-book.component.css']
})
export class CreateBookComponent implements OnInit {

  @ViewChild(AuthorsBooksComponent) authorBooks: AuthorsBooksComponent;
  @ViewChild(AuthorBooksUneditableComponent) uneditableBooks: AuthorBooksUneditableComponent;


  bookForm: FormGroup;

  litForms: string[] = ['Próza', 'Drama', 'Poezie'];

  bookNameControl = new FormControl(null, [Validators.required, Validators.maxLength(80)]);
  litFormControl = new FormControl(null,  [Validators.required]);
  litGenresControl = new FormControl();
  firstEditionControl = new FormControl(null, [Validators.required, Validators.pattern('^[0-9]*$')]);
  isCzechOrWorldControl = new FormControl(null, [Validators.required]);
  authorNameControl = new FormControl(null,  [Validators.maxLength(80)]);
  authorInfoControl = new FormControl();
  authorBooksDbControl = new FormControl();
  authorBooksOtherControl = new FormControl();
  historyContextControl = new FormControl();
  literaryContextControl = new FormControl();
  contemporaryAuthorsControl = new FormControl();
  themeControl = new FormControl();
  motivesControl = new FormControl();
  plotControl = new FormControl();
  spaceControl = new FormControl();
  timeControl = new FormControl();
  charactersControl = new FormControl();
  compositionControl = new FormControl();
  extraInfoControl = new FormControl();

  controls = [this.bookNameControl, this.litFormControl, this.litGenresControl, this.firstEditionControl, this.isCzechOrWorldControl,
    this.authorNameControl, this.authorInfoControl, this.authorBooksOtherControl,
    this.historyContextControl, this.literaryContextControl,
    this.contemporaryAuthorsControl, this.themeControl, this.motivesControl, this.plotControl, this.spaceControl,
    this.timeControl, this.charactersControl, this.compositionControl, this.extraInfoControl];

  progressCounter = 0;
  progressBarValue = 0;
  progressBarColor;
  maxProgress = 17;

  constructor(private formBuilder: FormBuilder,
              private bookService: BookService,
              private router: Router,
              private dialog: MatDialog) {
  }

  ngOnInit(): void {
    this.bookForm = this.formBuilder.group({
      bookName: this.bookNameControl,
      litForm: this.litFormControl,
      litGenres: this.litGenresControl,
      firstEdition: this.firstEditionControl,
      isCzechOrWorld: this.isCzechOrWorldControl,
      authorName: this.authorNameControl,
      authorInfo: this.authorInfoControl,
      authorBooksDb: this.authorBooksDbControl,
      authorBooksOther: this.authorBooksOtherControl,
      historyContext: this.historyContextControl,
      literaryContext: this.literaryContextControl,
      contemporaryAuthors: this.contemporaryAuthorsControl,
      theme: this.themeControl,
      motives: this.motivesControl,
      space: this.spaceControl,
      time: this.timeControl,
      plot: this.plotControl,
      characters: this.charactersControl,
      composition: this.compositionControl,
      extraInfo: this.extraInfoControl
    });
  }

  authorChangedHandler(author: AuthorDetail) {
    if (author.id !== null) {
      this.authorNameControl.setValue(author.name);
      this.authorInfoControl.setValue(author.info);
      this.authorBooksDbControl.setValue(author.dbBooks);
      this.authorBooksOtherControl.setValue(author.otherBooks);
      if (this.uneditableBooks !== undefined) {
        this.uneditableBooks.setBooks(author.dbBooks);
      }
      if (this.authorBooks !== undefined) {
        this.authorBooks.setBooks(author.otherBooks);
      }
      this.authorNameControl.disable();
    } else{
      this.authorNameControl.setValue(null);
      this.authorInfoControl.setValue(null);
      this.authorBooksDbControl.setValue(null);
      this.authorBooksOtherControl.setValue(null);
      this.uneditableBooks.setBooks(null);
      this.authorBooks.setBooks(null);
      this.authorNameControl.enable();
    }
  }

  genresChangedHandler(genres: string[]) {
    this.litGenresControl.setValue(genres);
  }

  booksChangedHandler(otherBooks: string[]) {
    this.authorBooksOtherControl.setValue(otherBooks);
  }

  authorsChangedHandler(authors: string[]) {
    this.contemporaryAuthorsControl.setValue(authors);
  }

  charactersChangedHandler(characters: BookCharacter[]) {
    this.charactersControl.setValue(characters);
  }

  copyContext(type: string){
    const dialogConfig = new MatDialogConfig();

    dialogConfig.autoFocus = true;
    dialogConfig.data = {
      contextType: type,
      name: this.authorNameControl.value,
      firstEdition: this.firstEditionControl.value,
      isCzech: this.isCzechOrWorldControl
    };

    const dialogRef = this.dialog.open(ContextDialogComponent, dialogConfig);

    dialogRef.afterClosed().subscribe(
      data => {
        if (type === 'history'){
          this.historyContextControl.setValue(data);
        } else if (type === 'literary'){
          this.literaryContextControl.setValue(data);
        }
      });
  }

  progressChanged(){
    this.progressCounter = 0;
    this.controls.forEach((e) => {
      if (e.value !== null && e.value !== '' && e.value !== []){
        this.progressCounter++;
      }
    });
    if (this.authorNameControl.disabled === true){
      this.progressCounter++;
    }
    this.progressBarValue = this.progressCounter * 5.3;
    if (this.progressBarValue < 33){
      this.progressBarColor = 'warn';
    } else if (this.progressBarValue < 66){
      this.progressBarColor = 'accent';
    } else {
      this.progressBarColor = 'primary';
    }
  }

  createBook() {
    this.authorNameControl.enable();
    this.bookService.createBook(this.bookForm.value);
  }

  isAuthorFilled(): boolean{
    if (this.authorNameControl.value === null || this.authorNameControl.value === ''){
      this.maxProgress = 16;
      return false;
    } else {
      this.maxProgress = 19;
      return true;
    }
  }
}
