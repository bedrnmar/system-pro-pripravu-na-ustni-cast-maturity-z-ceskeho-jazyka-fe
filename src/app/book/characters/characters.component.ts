import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {FormControl, Validators} from '@angular/forms';
import {BookCharacter} from '../../dto/bookCharacter';

@Component({
  selector: 'app-characters',
  templateUrl: './characters.component.html',
  styleUrls: ['./characters.component.css']
})
export class CharactersComponent implements OnInit {
  characters: BookCharacter[] = [];

  @Output() charactersChanged: EventEmitter<BookCharacter[]> = new EventEmitter();

  charNameControl = new FormControl(null, [Validators.required]);
  charDescriptionControl = new FormControl(null, [Validators.required]);

  characterAdding = false;

  constructor() { }

  ngOnInit(): void {
  }

  removeCharacter(character: BookCharacter){
    const index = this.characters.indexOf(character);

    if (index >= 0) {
      this.characters.splice(index, 1);
    }
    this.charactersChanged.emit(this.characters);
  }

  addCharacter(){
    const newCharacter: BookCharacter = {
      name: this.charNameControl.value,
      description: this.charDescriptionControl.value
    };
    this.characters.push(newCharacter);
    this.charNameControl.setValue(null);
    this.charDescriptionControl.setValue(null);
    this.charactersChanged.emit(this.characters);
    this.characterAdding = false;
  }

  showCharacterAdding(): void{
    this.characterAdding = true;
  }

  setCharacters(characters: BookCharacter[]) {
    for (const character of characters) {
      this.characters.push(character);
    }
  }
}
