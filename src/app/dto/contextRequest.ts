export interface ContextRequest{
  authorName: string;
  firstEdition: number;
  isCzech: boolean;
}
