export interface BookView {
  id: number;
  name: string;
  author: string;
  form: string;
  category: string;
}
