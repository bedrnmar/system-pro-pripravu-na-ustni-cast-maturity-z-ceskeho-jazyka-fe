import {BookCharacter} from './bookCharacter';

export interface Book{
  id: number;
  bookName: string;
  litForm: string;
  litGenres: string[];
  firstEdition: number;
  isCzechOrWorld: string;
  authorName: string;
  authorInfo: string;
  authorBooksDb: string[];
  authorBooksOther: string[];
  historyContext: string;
  literaryContext: string;
  contemporaryAuthors: string[];
  theme: string;
  motives: string;
  space: string;
  time: string;
  plot: string;
  characters: BookCharacter[];
  composition: string;
  extraInfo: string;
}
