export interface BookCharacter{
  name: string;
  description: string;
}
