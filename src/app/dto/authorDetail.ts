export interface AuthorDetail{
  id: number;
  name: string;
  info: string;
  dbBooks: string[];
  otherBooks: string[];
}
