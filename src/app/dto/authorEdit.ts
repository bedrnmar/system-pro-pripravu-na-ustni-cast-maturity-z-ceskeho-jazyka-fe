export interface AuthorEdit {
  id: number;
  name: string;
  info: string;
  dbBooks: string[];
  otherBooks: string[];
}
