import {Component, EventEmitter, Input, Output} from '@angular/core';

@Component({
  selector: 'app-slider-buttons',
  templateUrl: './slider-buttons.component.html',
  styleUrls: ['./slider-buttons.component.css']
})
export class SliderButtonsComponent {

  @Input() hasPrevious = true;
  @Input() hasNext = true;
  @Input() isEnd = false;

  @Output() submitBook: EventEmitter<null> = new EventEmitter();

  createBook() {
    this.submitBook.emit();
  }

}
