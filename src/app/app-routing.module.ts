import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {CreateBookComponent} from './book/create-book/create-book.component';
import {PageNotFoundComponent} from './page-not-found/page-not-found.component';
import {HomeComponent} from './home/home.component';
import {BookListComponent} from './book/book-list/book-list.component';
import {BookDetailComponent} from './book/book-detail/book-detail.component';
import {EditBookComponent} from './book/edit-book/edit-book.component';
import {AuthorListComponent} from './author/author-list/author-list.component';
import {CreateAuthorComponent} from './author/create-author/create-author.component';
import {AuthorDetailComponent} from './author/author-detail/author-detail.component';
import {EditAuthorComponent} from './author/edit-author/edit-author.component';

const routes: Routes = [
  { path: '', component: HomeComponent},
  { path: 'create-book', component: CreateBookComponent},
  { path: 'book-list', component: BookListComponent},
  { path: 'book-detail/:id', component: BookDetailComponent},
  { path: 'edit-book/:id', component: EditBookComponent},
  { path: 'author-list', component: AuthorListComponent},
  { path: 'create-author', component: CreateAuthorComponent},
  { path: 'author-detail/:id', component: AuthorDetailComponent},
  { path: 'edit-author/:id', component: EditAuthorComponent},
  { path: '**', component: PageNotFoundComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
