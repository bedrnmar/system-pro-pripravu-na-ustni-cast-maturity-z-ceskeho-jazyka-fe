import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Params, Router} from '@angular/router';
import {MatDialog, MatDialogConfig} from '@angular/material/dialog';
import {ConfirmDialogComponent} from '../../dialogs/confirm-dialog/confirm-dialog.component';
import {AuthorService} from '../author.service';
import {NotificationService} from '../../services/notification.service';
import {AuthenticationService} from '../../services/authentication.service';
import {AuthorDetail} from '../../dto/authorDetail';

@Component({
  selector: 'app-author-detail',
  templateUrl: './author-detail.component.html',
  styleUrls: ['./author-detail.component.css', '../../book/book-detail/book-detail.component.css']
})
export class AuthorDetailComponent implements OnInit {

  id: number;
  author: AuthorDetail;
  allBooks: string[];

  constructor(private router: Router,
              private route: ActivatedRoute,
              private authorService: AuthorService,
              private dialog: MatDialog,
              private notificationService: NotificationService,
              private auth: AuthenticationService){
    if (this.route.routeConfig.component.name === this.constructor.name) {
      this.route.params.subscribe((params: Params) => {
        this.id = params.id;
      });
    }
    this.getAuthor();
  }

  ngOnInit(): void {

  }

  getAuthor(){
    this.authorService.getAuthor(this.id)
      .subscribe(
        data => {
          this.author = data;
          this.allBooks = this.getAuthorBooks();
        },
        error => {
          console.error('Couldn\'t get because', error);
          if (error.status !== 401 && error.status !== 200) {
            this.notificationService.notifyError('Chyba při načítání. ' + error.status);
          }
          this.router.navigate(['/author-list']);
        }
    );
  }

  showArrayInBlock(array: string[]): string{
    let text = '';

    if (array !== undefined) {
      for (let i = 0; i < array.length; i++) {
        text = text + array[i];
        if (i !== array.length - 1) {
          text = text + ', ';
        }
      }
    }
    return text;
  }

  editAuthor(): void{
    this.router.navigate(['./edit-author', this.id]);
  }

  deleteAuthor(){
    const dialogConfig = new MatDialogConfig();

    dialogConfig.autoFocus = true;
    dialogConfig.width = '500px';
    dialogConfig.data = {
      title: 'Smazání autora',
      text: 'Opravdu chcete smazat autora ' + this.author.name + '?'
    };

    const dialogRef = this.dialog.open(ConfirmDialogComponent, dialogConfig);

    dialogRef.afterClosed().subscribe(
      data => {
        if (data === true) {
          this.authorService.deleteAuthor(this.id);
        }
      });
  }

  getAuthorName(): string{
    if (this.author !== undefined){
      return this.author.name;
    }
    return '';
  }

  getAuthorInfo(): string{
    if (this.author !== undefined){
      return this.author.info;
    }
    return '';
  }

  getAuthorBooks(): string[]{
    if (this.author !== undefined){
      if (this.author.dbBooks !== undefined && this.author.otherBooks !== undefined){
        return this.author.dbBooks.concat(this.author.otherBooks);
      } else if (this.author.otherBooks === undefined && this.author.dbBooks === undefined){
        return [];
      } else if (this.author.dbBooks !== undefined){
        return this.author.dbBooks;
      } else {
        return this.author.dbBooks;
      }
    }
    return [];
  }

  isUserLoggedIn(): boolean{
    return this.auth.isUserLogged();
  }

  getTooltip(): string{
    if (this.author !== undefined) {
      if (this.author.dbBooks === undefined) {
        return 'Autor nelze smazat, protože v databázi jsou uložena některá jeho díla';
      } else {
        return 'Smaže autora';
      }
    }
    return '';
  }

  canBeDeleted(): boolean{
    if (this.author !== undefined){
      return this.author.dbBooks.length >= 1;
    }
    return true;
  }

}
