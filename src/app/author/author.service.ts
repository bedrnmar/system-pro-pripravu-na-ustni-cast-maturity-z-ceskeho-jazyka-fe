import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {NotificationService} from '../services/notification.service';
import {AuthorEdit} from '../dto/authorEdit';
import {Router} from '@angular/router';
import {Observable} from 'rxjs';
import {environment} from '../dto/environment';
import {AuthorDetail} from '../dto/authorDetail';

@Injectable({
  providedIn: 'root'
})
export class AuthorService {

  constructor(private http: HttpClient,
              private notificationService: NotificationService,
              private router: Router) { }

  getAuthors(): Observable<AuthorDetail[]>{
     return this.http.get<AuthorDetail[]>(environment.publicUrl + '/author-list');
  }

  getAuthor( id: number): Observable<AuthorDetail>{
    return this.http.get<AuthorDetail>(environment.publicUrl + '/author-detail?id=' + id);
  }

  createAuthor( author: AuthorDetail){
    this.http.post<AuthorDetail>(environment.publicUrl + '/create-author', author)
      .subscribe(
        data => {
          console.log('Author created successfully');
          this.notificationService.notifySuccess('Úspěšně vytvořeno.');
          this.router.navigate(['/author-list']);
        },
        error => {
          console.error('Couldn\'t post because', error);
          if (error.status !== 401 && error.status !== 200) {
            this.notificationService.notifyError('Chyba při ukládání. ' + error.status);
          }
        }
      );
  }

  updateAuthor( author: AuthorDetail){
    return this.http.put<AuthorDetail>(environment.publicUrl + '/edit-author', author);
  }

  deleteAuthor( id: number) {
    this.http.delete(environment.privateUrl + '/delete-author?id=' + id)
      .subscribe(
        data => {
          this.router.navigate(['/author-list']);
          this.notificationService.notifySuccess('Úspěšně smazáno.');
        },
        error => {
          console.error('Couldn\'t post because', error);
          if (error.status !== 401 && error.status !== 200) {
            this.notificationService.notifyError('Chyba při ukládání. ' + error.status);
          }
        }
      );
  }
}
