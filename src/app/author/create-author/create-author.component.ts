import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {AuthorService} from '../author.service';

@Component({
  selector: 'app-create-author',
  templateUrl: './create-author.component.html',
  styleUrls: ['./create-author.component.css', '../../book/book-detail/book-detail.component.css']
})
export class CreateAuthorComponent implements OnInit {

  authorForm: FormGroup;

  authorNameControl = new FormControl(null, [Validators.required]);
  authorInfoControl = new FormControl(null, [Validators.required]);
  authorBooksControl = new FormControl(null, [Validators.required]);

  constructor(private fb: FormBuilder,
              private authorService: AuthorService) {
  }

  ngOnInit(): void {
    this.authorForm = this.fb.group({
      name: this.authorNameControl,
      info: this.authorInfoControl,
      books: this.authorBooksControl
    });
  }

  isFilled(): boolean{
    return this.authorBooksControl.errors === null
      && this.authorNameControl.errors === null
      && this.authorInfoControl.errors === null;
  }

  booksChangedHandler(books: string[]) {
    this.authorBooksControl.setValue(books);
  }

  submitAuthor(){
    this.authorService.createAuthor(this.authorForm.value);
  }
}
