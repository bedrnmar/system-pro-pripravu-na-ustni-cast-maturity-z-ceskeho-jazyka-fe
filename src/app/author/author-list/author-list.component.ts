import {AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {Router} from '@angular/router';
import {MatTableDataSource} from '@angular/material/table';
import {AuthorService} from '../author.service';
import {NotificationService} from '../../services/notification.service';
import {AuthorDetail} from '../../dto/authorDetail';

@Component({
  selector: 'app-author-list',
  templateUrl: './author-list.component.html',
  styleUrls: ['./author-list.component.css']
})
export class AuthorListComponent implements OnInit, AfterViewInit {
  displayedColumns: string[] = ['name', 'books', 'books2'];
  dataSource;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(private authorService: AuthorService,
              private router: Router,
              private notificationService: NotificationService) {}

  ngOnInit(): void {
    this.dataSource = new MatTableDataSource<AuthorDetail>();
    this.authorService.getAuthors()
      .subscribe(
        data => {
          this.dataSource.data = data;
        },
        error => {
          console.error('Couldn\'t post because', error);
          if (error.status !== 401 && error.status !== 200) {
             this.notificationService.notifyError('Chyba při načítání. ' + error.status);
          }
        }
      );
  }

  ngAfterViewInit(): void {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  openAuthorDetail(authorId: number){
    this.router.navigate(['./author-detail', authorId]);
  }

  showArrayInBlock(array: string[]): string{
    let text = '';

    if (array !== undefined) {
      for (let i = 0; i < array.length; i++) {
        text = text + array[i];
        if (i !== array.length - 1) {
          text = text + ', ';
        }
      }
    }
    return text;
  }
}
