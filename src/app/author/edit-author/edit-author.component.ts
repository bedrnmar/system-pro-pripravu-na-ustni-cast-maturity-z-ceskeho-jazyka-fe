import {Component, Input, OnInit, ViewChild} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {AuthorService} from '../author.service';
import {AuthorEdit} from '../../dto/authorEdit';
import {ActivatedRoute, Params, Router} from '@angular/router';
import {NotificationService} from '../../services/notification.service';
import {AuthorDetail} from '../../dto/authorDetail';
import {AuthorsBooksComponent} from '../../chips/authors-books/authors-books.component';
import {AuthorBooksUneditableComponent} from '../../chips/author-books-uneditable/author-books-uneditable.component';

@Component({
  selector: 'app-edit-author',
  templateUrl: './edit-author.component.html',
  styleUrls: ['./edit-author.component.css', '../../book/book-detail/book-detail.component.css', '../create-author/create-author.component.css']
})
export class EditAuthorComponent implements OnInit {

  @ViewChild(AuthorsBooksComponent) authorBooks: AuthorsBooksComponent;
  @ViewChild(AuthorBooksUneditableComponent) uneditableBooks: AuthorBooksUneditableComponent;

  @Input() id: number;
  author: AuthorEdit;
  authorDetail: AuthorDetail = null;

  authorForm: FormGroup;

  authorIdControl = new FormControl();
  authorNameControl = new FormControl(null, [Validators.required]);
  authorInfoControl = new FormControl(null, [Validators.required]);
  authorDbBooksControl = new FormControl(null);
  authorOtherBooksControl = new FormControl(null, [Validators.required]);

  constructor(private fb: FormBuilder,
              private authorService: AuthorService,
              private router: Router,
              private route: ActivatedRoute,
              private notificationService: NotificationService) {
  }

  ngOnInit(): void {
    if (this.route.routeConfig.component.name === this.constructor.name) {
      this.route.params.subscribe((params: Params) => {
        this.id = params.id;
      });
    }
    this.authorForm = this.fb.group({
      id: this.authorIdControl,
      name: this.authorNameControl,
      info: this.authorInfoControl,
      dbBooks: this.authorDbBooksControl,
      otherBooks: this.authorOtherBooksControl
    });
    this.getAuthor();
  }

  getAuthor(){
    this.authorService.getAuthor(this.id)
      .subscribe(
        data => {
          this.author = data;
          this.authorIdControl.setValue(this.author.id);
          this.authorNameControl.setValue(this.author.name);
          this.authorInfoControl.setValue(this.author.info);
          this.authorDbBooksControl.setValue(this.author.dbBooks);
          this.authorOtherBooksControl.setValue(this.author.otherBooks);
          this.uneditableBooks.setBooks(this.author.dbBooks);
          this.authorBooks.setBooks(this.author.otherBooks);
        },
        error => {
          console.error('Couldn\'t get because', error);
          if (error.status !== 401 && error.status !== 200) {
            this.notificationService.notifyError('Chyba při načítání. ' + error.status);
          }
        }
    );

  }

  submitAuthor(){
    const authorSubmit = {
      id: this.authorIdControl.value,
      name: this.authorNameControl.value,
      info: this.authorInfoControl.value,
      dbBooks: this.authorDbBooksControl.value,
      otherBooks: this.authorOtherBooksControl.value
    };

    this.authorService.updateAuthor(authorSubmit)
      .subscribe(
        data => {
          this.notificationService.notifySuccess('Autor byl změněn.');
          this.router.navigate(['/author-detail', this.author.id]);
        },
        error => {
          console.error('Couldn\'t get because', error);
          if (error.status !== 401 && error.status !== 200) {
            this.notificationService.notifyError('Chyba při načítání. ' + error.status);
          }
        }
      );
  }

  getAuthorDbBooks(){
    if (this.author !== undefined){
      return this.author.dbBooks;
    }
    return [];
  }

  getAuthorOtherBooks(){
    if (this.author !== undefined){
      return this.author.otherBooks;
    }
    return [];
  }

  booksChangedHandler(books: string[]) {
    this.authorOtherBooksControl.setValue(books);
  }
}
