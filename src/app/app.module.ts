import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { BookListComponent } from './book/book-list/book-list.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { AuthorListComponent } from './author/author-list/author-list.component';
import {BookDetailComponent} from './book/book-detail/book-detail.component';
import {BookService} from './book/book.service';
import {HttpClientModule} from '@angular/common/http';
import { ToolbarComponent } from './toolbar/toolbar.component';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatIconModule} from '@angular/material/icon';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {MatButtonModule} from '@angular/material/button';
import {MatDialogModule} from '@angular/material/dialog';
import { RegisterDialogComponent } from './dialogs/register-dialog/register-dialog.component';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatInputModule} from '@angular/material/input';
import { LoginDialogComponent } from './dialogs/login-dialog/login-dialog.component';
import { ConfirmDialogComponent } from './dialogs/confirm-dialog/confirm-dialog.component';
import {AppRoutingModule} from './app-routing.module';
import { CreateBookComponent } from './book/create-book/create-book.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { HomeComponent } from './home/home.component';
import { MatSidenavModule} from '@angular/material/sidenav';
import {MatListModule} from '@angular/material/list';
import {MatStepperModule} from '@angular/material/stepper';
import {MatRadioModule} from '@angular/material/radio';
import {MatAutocompleteModule} from '@angular/material/autocomplete';
import {MatChipsModule} from '@angular/material/chips';
import { GenresChipsComponent } from './chips/genres-chips/genres-chips.component';
import { AuthorsBooksComponent } from './chips/authors-books/authors-books.component';
import { SliderButtonsComponent } from './slider-buttons/slider-buttons.component';
import {MatTooltipModule} from '@angular/material/tooltip';
import { ContemporaryAuthorsChipsComponent } from './chips/contemporary-authors-chips/contemporary-authors-chips.component';
import { FindAuthorComponent } from './book/find-author/find-author.component';
import { CharactersComponent } from './book/characters/characters.component';
import {MatSnackBarModule} from '@angular/material/snack-bar';
import {MatProgressBarModule} from '@angular/material/progress-bar';
import {MatTableModule} from '@angular/material/table';
import {MatPaginatorModule} from '@angular/material/paginator';
import {MatSortModule} from '@angular/material/sort';
import {MatDividerModule} from '@angular/material/divider';
import { EditBookComponent } from './book/edit-book/edit-book.component';
import { CreateAuthorComponent } from './author/create-author/create-author.component';
import { AuthorDetailComponent } from './author/author-detail/author-detail.component';
import { EditAuthorComponent } from './author/edit-author/edit-author.component';
import {authInterceptorProviders} from './authentication/JwtInterceptor';
import { ContextDialogComponent } from './dialogs/context-dialog/context-dialog.component';
import {MatSelectModule} from '@angular/material/select';
import { AuthorBooksUneditableComponent } from './chips/author-books-uneditable/author-books-uneditable.component';
import {MatCardModule} from '@angular/material/card';
import {MatGridListModule} from '@angular/material/grid-list';

@NgModule({
  declarations: [
    AppComponent,
    BookListComponent,
    BookDetailComponent,
    AuthorListComponent,
    ToolbarComponent,
    RegisterDialogComponent,
    LoginDialogComponent,
    ConfirmDialogComponent,
    CreateBookComponent,
    PageNotFoundComponent,
    HomeComponent,
    GenresChipsComponent,
    AuthorsBooksComponent,
    SliderButtonsComponent,
    ContemporaryAuthorsChipsComponent,
    FindAuthorComponent,
    CharactersComponent,
    EditBookComponent,
    CreateAuthorComponent,
    AuthorDetailComponent,
    EditAuthorComponent,
    ContextDialogComponent,
    AuthorBooksUneditableComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    MatToolbarModule,
    MatIconModule,
    MatButtonModule,
    BrowserAnimationsModule,
    MatDialogModule,
    MatFormFieldModule,
    MatInputModule,
    ReactiveFormsModule,
    AppRoutingModule,
    MatSidenavModule,
    MatListModule,
    MatStepperModule,
    MatRadioModule,
    MatAutocompleteModule,
    MatChipsModule,
    MatTooltipModule,
    MatSnackBarModule,
    MatProgressBarModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    MatDividerModule,
    MatSelectModule,
    MatCardModule
  ],
  providers: [BookService, authInterceptorProviders],
  bootstrap: [AppComponent]
})
export class AppModule { }
