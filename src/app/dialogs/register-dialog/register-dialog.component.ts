import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormControl, FormGroup, ValidationErrors, Validators} from '@angular/forms';
import {MatDialogRef} from '@angular/material/dialog';

@Component({
  selector: 'app-register-dialog',
  templateUrl: './register-dialog.component.html',
  styleUrls: ['./register-dialog.component.css']
})
export class RegisterDialogComponent implements OnInit {
  registerForm: FormGroup;
  hide = true;

  usernameControl = new FormControl(null, [Validators.required]);
  passwordControl = new FormControl(null, [Validators.required]);

  constructor(private fb: FormBuilder, private dialogRef: MatDialogRef<RegisterDialogComponent>) {

    this.registerForm = this.fb.group({
      username: this.usernameControl,
      password: this.passwordControl
    });
  }

  get email() {return this.registerForm.get('email'); }
  get password() {return this.registerForm.get('password'); }

  ngOnInit(): void {

  }

  close() {
    this.dialogRef.close();
  }

  register() {
    this.dialogRef.close(this.registerForm.value);
  }

}
