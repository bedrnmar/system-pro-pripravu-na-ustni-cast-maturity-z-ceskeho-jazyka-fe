import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {MatDialogRef} from '@angular/material/dialog';

@Component({
  selector: 'app-login-dialog',
  templateUrl: './login-dialog.component.html',
  styleUrls: ['./login-dialog.component.css']
})
export class LoginDialogComponent implements OnInit {

  loginForm: FormGroup;
  hide = true;

  usernameControl = new FormControl(null, [Validators.required]);
  passwordControl = new FormControl(null, [Validators.required]);

  constructor(private fb: FormBuilder, private dialogRef: MatDialogRef<LoginDialogComponent>) {

    this.loginForm = this.fb.group({
      username: this.usernameControl,
      password: this.passwordControl
    });
  }

  get username() {return this.loginForm.get('username'); }
  get password() {return this.loginForm.get('password'); }

  ngOnInit(): void {

  }

  close() {
    this.dialogRef.close();
  }

  login() {
    this.dialogRef.close(this.loginForm.value);
  }

}
