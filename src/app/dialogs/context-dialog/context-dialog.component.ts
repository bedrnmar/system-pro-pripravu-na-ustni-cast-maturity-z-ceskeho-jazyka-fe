import {Component, Inject, Input, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {BookContext} from '../../dto/bookContext';
import {AuthorDetail} from '../../dto/authorDetail';
import {map, startWith} from 'rxjs/operators';
import {BookService} from '../../book/book.service';
import {ContextRequest} from '../../dto/contextRequest';

@Component({
  selector: 'app-context-dialog',
  templateUrl: './context-dialog.component.html',
  styleUrls: ['./context-dialog.component.css']
})
export class ContextDialogComponent implements OnInit {

  nameControl = new FormControl();
  contextControl = new FormControl(null, [Validators.required]);
  title: string;

  constructor(private dialogRef: MatDialogRef<ContextDialogComponent>,
              @Inject(MAT_DIALOG_DATA) dataObtained,
              private bookService: BookService) {
    if (dataObtained.contextType === 'history'){
      this.title = 'Kopírování historického kontextu';
    } else if (dataObtained.contextType === 'literary'){
      this.title = 'Kopírování historického kontextu';
    }
    const isCzechObtained = dataObtained.isCzech === 'czech';
    const contextRequest: ContextRequest = {
      authorName: dataObtained.name,
      firstEdition: dataObtained.firstEdition,
      isCzech: isCzechObtained
    };
    this.bookService.getContexts(dataObtained.contextType, contextRequest)
      .subscribe(
        data => {
          this.titles = data;
        },
        error => {
          this.titles = [];
          console.error('Couldn\'t post because', error);
        }
      );
  }

  titles: BookContext[];

  ngOnInit(): void {

  }

  getContexts(){

  }

close() {
    this.dialogRef.close();
  }

choose() {
    this.dialogRef.close(this.contextControl.value);
  }
}
